#pragma once

#include <type_traits>
#include <tuple>
#include <optional>

#include "getNamedTypeParts.hpp"

template<typename T, typename... Ts>
constexpr bool contains()
{
	return std::disjunction_v<std::is_same<T, Ts>...>;
}

template<class, class>
class AnyOrderFunction;

constexpr decltype(std::nullopt) noDefault = std::nullopt;

template<class ID, class Ret, class... Args>
class AnyOrderFunction <ID, Ret(Args...)>{
	static_assert(std::conjunction_v<isNamedType<Args>...>, "all parameters must be NamedTypes");
public:
	constexpr AnyOrderFunction() = default;

	constexpr static AnyOrderFunction withDefaults(std::optional<std::decay_t<NamedType_t<Args>>>... defaults)
	{
		return {defaults...};
	}

	Ret impl(NamedType_t<Args>... args) const;

	template<class... Args2>
	Ret operator() (Args2... args2) const
	{
		auto argTuple = std::tuple<Args2...>(args2...);
		static_assert(std::is_same_v<Ret, decltype(impl(pickOrDefault<Args>(argTuple)...))>);
		return impl(pickOrDefault<Args>(argTuple)...);
	}

private:
	constexpr AnyOrderFunction(std::optional<std::decay_t<NamedType_t<Args>>>... defaults) : defaults_(defaults...)
	{}

	const std::tuple<std::optional<decayNamedType_t<Args>>...> defaults_;

	template<typename TypeToPick, typename... CallArgs>
	auto pickOrDefault(const std::tuple<CallArgs...>& args) const
	{
		static_assert(contains<TypeToPick, Args...>());

		if constexpr(contains<TypeToPick, CallArgs...>())
		{
			return std::get<TypeToPick>(args).get();
		}
		else
		{
			auto pickedDefault = std::get<std::optional<decayNamedType_t<TypeToPick>>>(defaults_);
			if(pickedDefault)
			{
				return (*pickedDefault).get();
			}
			throw std::runtime_error{"no default for this type"};
		}
	}
};

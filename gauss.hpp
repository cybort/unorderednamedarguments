#pragma once
#include <utility>
#include <named_type.hpp>

#include "AnyOrderFunction.hpp"

using Mean = fluent::NamedType<double, struct MeanID>;
constexpr Mean::argument mean;

using StdDev = fluent::NamedType<double, struct StdDevID>;
constexpr StdDev::argument stdDev;

using XVal = fluent::NamedType<double, struct XValID>;
constexpr XVal::argument xVal;

using Gauss = AnyOrderFunction<struct GaussID, double(Mean, StdDev, XVal)>;
const Gauss gauss = Gauss::withDefaults(0.0, 1.0, noDefault);

#pragma once
#include <named_type.hpp>

template<class>
struct getID;

template<class T>
using getID_t = typename getID<T>::type;

template<class T, class ID, template<class> class... Modifiers>
struct getID<fluent::NamedType<T,ID, Modifiers...>>
{
	using type = ID;
};

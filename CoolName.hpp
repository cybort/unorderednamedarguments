#pragma once

#include <string>
#include <utility>

#include <named_type.hpp>

#include "pick.hpp"
#include "AnyOrderFunction.hpp"

/// Named Types
using FirstName = fluent::NamedType<std::string const&, struct FirstNameID>;
constexpr FirstName::argument firstName;

using LastName = fluent::NamedType<std::string const&, struct LastNameID>;
constexpr LastName::argument lastName;

using Drink = fluent::NamedType<std::string const&, struct DrinkID>;
constexpr Drink::argument drink;

enum class PREPARATION{SHAKEN, STIRRED};
using Preparation = fluent::NamedType<PREPARATION, struct PreparationID>;
constexpr Preparation::argument preparation;

/// approximate FluentCpp implementation
void coolName1_impl(FirstName theFirstName, LastName theLastName, Drink theDrink, Preparation thePrep);

template<class... Ts>
void coolName1(Ts... ts)
{
	auto args = std::tuple<Ts...>(ts...);
	coolName1_impl(pick<FirstName>(args), pick<LastName>(args), pick<Drink>(args), pick<Preparation>(args));
}

/// AnyOrderFunction implementation
using CoolName2 = AnyOrderFunction<struct CoolName2ID, void(FirstName, LastName, Drink, Preparation)>;
const CoolName2 coolName2 = CoolName2::withDefaults(noDefault, noDefault, "Vodka Martini", PREPARATION::SHAKEN);

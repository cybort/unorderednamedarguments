#pragma once

#include <type_traits>
#include <tuple>

template<class T>
struct Type{};

template<template<class> class Filter, class Check, class... Ts >
constexpr unsigned getIndex(unsigned I, Type<Check>, Type<Ts>... remainder)
{
	if constexpr(Filter<Check>::value)
	{
		return I;
	}
	else
	{
		if constexpr (sizeof...(Ts) == 0)
		{
			static_assert(false);
		}
		{
			return getIndex<Filter>(I + 1, remainder...);
		}
	}
}

template<template<class> class Filter, class... Ts>
auto getQualified(std::tuple<Ts...>& tuple)
{
	constexpr unsigned FirstFulfilledIndex = getIndex<Filter>(0u, Type<Ts>{}...);
	return std::get<FirstFulfilledIndex>(tuple);
}

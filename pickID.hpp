#pragma once

#include <type_traits>

#include <named_type.hpp>

#include "getQualified.hpp"


template<class T, class ID>
struct has_id : std::false_type {};

template<class T, class ID, template<class> class... mods >
struct has_id<fluent::NamedType<T, ID, mods...>, ID> : std::true_type {};

template<class ID>
struct has_IDToPick
{
	template<class T>
	struct type : has_id<T, ID> {};
};

template<typename IDToPick, typename... Types>
auto pickID(std::tuple<Types...>& args)
{
	return getQualified<has_IDToPick<IDToPick>::type>(args);
}

